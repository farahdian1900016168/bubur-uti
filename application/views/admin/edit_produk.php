<div class="container-fluid">
    <h3><i class="fas fa-edit"></i>Edit Produk</h3>
    <?php foreach($produk as $prd) : ?>
        <form method="post" action="<?php echo base_url().'admin/data_produk/update' ?>">
            <div class="for-group">
                <label>Nama produk</label>
                <input type="hidden" name="id_produk" class="form-control" value="<?php echo $prd->id_product ?>">
                <input type="text" name="nama_produk" class="form-control" value="<?php echo $prd->nama_product ?>">  
                
            </div>
            <div class="for-group">
                <label>Harga</label>
                <input type="text" name="harga" class="form-control" value="<?php echo $prd->harga ?>">  
            </div>
            <button type="submit" class="btn btn-primary mt-3">Simpan</button>
        </form>
    <?php endforeach; ?>
</div>